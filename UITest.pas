unit UITest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, xmldom, XMLIntf, msxmldom, XMLDoc, ComCtrls, StdCtrls, Buttons, ShellAPI,
  ExtCtrls;

type

  TForm1 = class(TForm)
    xml1: TXMLDocument;
    ListView1: TListView;
    Label1: TLabel;
    PNEdit: TEdit;
    Label2: TLabel;
    SNEdit: TEdit;
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    LANEdit: TEdit;
    Label4: TLabel;
    WlanEdit: TEdit;
    Label5: TLabel;
    BTEdit: TEdit;
    Label6: TLabel;
    keyEdit: TEdit;
    Label7: TLabel;
    LineEdit: TEdit;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    function DoTest(exec: String; passlog: String; IsSync: String; Index: Integer): Bool;
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

var
  timeEsp: Integer;
  IsPass: Bool;
  CS: TRTLCriticalSection;

type
  PSYNC = ^TSYNC;
  TSYNC = record
    exec: Pchar;
    passlog: Pchar;
    index: Integer;
  end;
{$R *.dfm}

function DoSyncTest(P: TSYNC):Integer; stdcall;
var
  sinfo :SHELLEXECUTEINFO;
begin
  sinfo.cbSize := SizeOf(SHELLEXECUTEINFO);
  sinfo.fMask := SEE_MASK_NOCLOSEPROCESS;
  sinfo.Wnd := Application.Handle;
  sinfo.lpVerb := nil;
  sinfo.lpFile :=p.exec;
  sinfo.lpParameters := '';
  sinfo.nShow := SW_SHOW;
  sinfo.hInstApp := 0;

  EnterCriticalSection(CS);
    Form1.ListView1.Items[p.index].Selected := True;
    Form1.ListView1.Selected.SubItems.Strings[1] := FormatDateTime('hh:nn:ss',Now());
  LeaveCriticalSection(CS);

  ShellExecuteEx(@sinfo);

  WaitForSingleObject(sinfo.hProcess, INFINITE);

//  EnterCriticalSection(CS);
    Form1.ListView1.Selected.SubItems.Strings[2] := FormatDateTime('hh:nn:ss ---',Now());
    If FileExists(p.passlog) then
      Form1.ListView1.Selected.SubItems.Strings[3] := 'Pass'
    else
      Form1.ListView1.Selected.SubItems.Strings[3] := 'Fail';
//  LeaveCriticalSection(CS);

//  Result:=0;
end;

function TForm1.DoTest(exec: String; passlog: String; IsSync: String; Index: Integer): Bool;
var
  sinfo :SHELLEXECUTEINFO;
begin
  sinfo.cbSize := SizeOf(SHELLEXECUTEINFO);
  sinfo.fMask := SEE_MASK_NOCLOSEPROCESS;
  sinfo.Wnd := Application.Handle;
  sinfo.lpVerb := nil;
  sinfo.lpFile :=PChar(exec);
  sinfo.lpParameters := '';
  sinfo.nShow := SW_SHOW;
  sinfo.hInstApp := 0;

//  EnterCriticalSection(CS);
    ListView1.Items[Index].Selected := True;
    ListView1.Selected.SubItems.Strings[1] := FormatDateTime('hh:nn:ss',Now());
//  LeaveCriticalSection(CS);

  ShellExecuteEx(@sinfo);

  WaitForSingleObject(sinfo.hProcess, INFINITE);
  
//  EnterCriticalSection(CS);
    ListView1.Selected.SubItems.Strings[2] := FormatDateTime('hh:nn:ss',Now());
    If FileExists(passlog) then
      ListView1.Selected.SubItems.Strings[3] := 'Pass'
    else
      ListView1.Selected.SubItems.Strings[3] := 'Fail';
//  LeaveCriticalSection(CS);
  
  Result:=true;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  	nl: IXMLNodeList;
	node: IXMLNode;
	item: TListItem;
  i: Integer;
begin
  InitializeCriticalSection(CS);
  timeEsp := 0;
  IsPass := True;

  if ParamCount >= 1 then StatusBar1.Panels[2].Text := ParamStr(1);

  if ParamCount >= 1 then begin
    if FileExists(ParamStr(1)) then
      xml1.LoadFromFile(ParamStr(1));
  end
  else
  begin
    if FileExists('item.xml') then xml1.LoadFromFile('item.xml');
  end;

  nl := xml1.DocumentElement.ChildNodes;
  ListView1.Items.Clear();

  for i:=0 to nl.Count-1 do
  begin
    item := listview1.Items.Add;
    node := nl.Nodes[i];
    item.Caption := IntToStr(item.Index);
    item.SubItems.Add(node['name']);
    item.SubItems.Add('=');
    item.SubItems.Add('=');
    item.SubItems.Add('=');
  end;

  Panel1.Caption := 'Waiting for start...';

end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if MessageBox(Application.Handle, 'Will you EXIT?', 'Warning',MB_YESNO OR MB_ICONWARNING OR MB_DEFBUTTON2 OR MB_SYSTEMMODAL) = 7 then abort;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
var
  	nl: IXMLNodeList;
	node: IXMLNode;
  i: Integer;
  exec: String;
  PassLog: String;
  IsSync: String;
  p: PSYNC;
  Id: Dword;
begin
  nl := xml1.DocumentElement.ChildNodes;
  getmem(p,sizeof(p));
  for i:=0 to nl.Count-1 do
  begin
    node := nl.Nodes[i];
    exec := node['exec'];
    Passlog := node['passlog'];
    IsSync := node['sync'];

    Panel1.Caption := node['name'];

    If IsSync = '1' then
    begin
      p^.exec := Pchar(exec);
      p^.passlog := Pchar(Passlog);
      p^.index := i;
      CreateThread(nil, 0, @DoSyncTest, p, 0, Id);
    end
    else
      DoTest(exec, passlog, IsSync, i);
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  myTxtFile: TextFile;
  temp: String;
begin
  Inc(timeEsp);
  StatusBar1.Panels[0].Text := FormatDateTime('YYYY-MM-dd hh:nn:ss',Now());
  StatusBar1.Panels[1].Text := 'Test time: ' + IntToStr(timeEsp) +'s';
  // Read pn.dat
  if FileExists('.\data\pn.dat') then begin
    AssignFile(myTxtFile, '.\data\pn.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    PnEdit.Text:= temp;
  end;

  // Read Sn.dat
  if FileExists('.\data\sn.dat') then begin
    AssignFile(myTxtFile, '.\data\sn.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    SNEdit.Text:= temp;
  end;

  // Read lanmac.dat
  if FileExists('.\data\lanmac.dat') then begin
    AssignFile(myTxtFile, '.\data\lanmac.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    lanEdit.Text:= temp;
  end;

  // Read wlanmac.dat
  if FileExists('.\data\wlanmac.dat') then begin
    AssignFile(myTxtFile, '.\data\wlanmac.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    wlanEdit.Text:= temp;
  end;

  // Read btmac.dat
  if FileExists('.\data\btmac.dat') then begin
    AssignFile(myTxtFile, '.\data\btmac.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    btEdit.Text:= temp;
  end;  

  // Read key.dat
  if FileExists('.\data\key.dat') then begin
    AssignFile(myTxtFile, '.\data\key.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    KeyEdit.Text:= temp;
  end;

  // Read line.dat
  if FileExists('.\data\line.dat') then begin
    AssignFile(myTxtFile, '.\data\line.dat');
    Reset(myTxtFile);
    readln(myTxtFile, temp);
    CloseFile(myTxtFile);
    lineEdit.Text:= temp;
  end;

  if FileExists('upass.log') then Application.Terminate;


end;

end.
