object Form1: TForm1
  Left = 339
  Top = 260
  Width = 853
  Height = 447
  Caption = 'UI Test Utiliy    Rev:1.0.0'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    845
    416)
  PixelsPerInch = 96
  TextHeight = 13
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 510
    Height = 393
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelWidth = 3
    BorderStyle = bsNone
    Columns = <
      item
        Caption = 'No.'
      end
      item
        Alignment = taCenter
        Caption = 'Item'
        Width = 150
      end
      item
        Alignment = taCenter
        Caption = 'Start'
        Width = 100
      end
      item
        Alignment = taCenter
        Caption = 'End'
        Width = 100
      end
      item
        Alignment = taCenter
        Caption = 'Result'
        Width = 80
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    GridLines = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    TabOrder = 0
    ViewStyle = vsReport
  end
  object BitBtn1: TBitBtn
    Left = 532
    Top = 344
    Width = 105
    Height = 33
    Anchors = [akTop, akRight]
    Caption = #24320#22987
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object GroupBox1: TGroupBox
    Left = 533
    Top = 80
    Width = 281
    Height = 249
    Anchors = [akTop, akRight]
    Caption = #22522#26412#20449#24687
    TabOrder = 2
    DesignSize = (
      281
      249)
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 60
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #25104#21697#26009#21495#65306
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 60
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #26426#22120#24207#21495#65306
    end
    object Label3: TLabel
      Left = 16
      Top = 88
      Width = 60
      Height = 13
      Caption = #32593#21345#22320#22336#65306
    end
    object Label4: TLabel
      Left = 16
      Top = 120
      Width = 60
      Height = 13
      Caption = #26080#32447#22320#22336#65306
    end
    object Label5: TLabel
      Left = 16
      Top = 152
      Width = 60
      Height = 13
      Caption = #34013#29273#22320#22336#65306
    end
    object Label6: TLabel
      Left = 16
      Top = 184
      Width = 60
      Height = 13
      Caption = #20135#21697#23494#38053#65306
    end
    object Label7: TLabel
      Left = 16
      Top = 216
      Width = 60
      Height = 13
      Caption = #29983#20135#32447#21035#65306
    end
    object PNEdit: TEdit
      Left = 88
      Top = 24
      Width = 169
      Height = 19
      Anchors = [akTop, akRight]
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 0
      Text = 'PNEdit'
    end
    object SNEdit: TEdit
      Left = 88
      Top = 56
      Width = 169
      Height = 19
      Anchors = [akTop, akRight]
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 1
      Text = 'SNEdit'
    end
    object LANEdit: TEdit
      Left = 88
      Top = 88
      Width = 169
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 2
      Text = 'LANEdit'
    end
    object WlanEdit: TEdit
      Left = 88
      Top = 120
      Width = 169
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 3
      Text = 'WlanEdit'
    end
    object BTEdit: TEdit
      Left = 88
      Top = 152
      Width = 169
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 4
      Text = 'BTEdit'
    end
    object keyEdit: TEdit
      Left = 88
      Top = 184
      Width = 169
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 5
      Text = 'keyEdit'
    end
    object LineEdit: TEdit
      Left = 88
      Top = 216
      Width = 169
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 6
      Text = 'LineEdit'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 397
    Width = 845
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 528
    Top = 8
    Width = 287
    Height = 57
    Anchors = [akTop, akRight]
    Caption = 'Loading...'
    Color = clActiveCaption
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -20
    Font.Name = 'FixedsysTTF'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object xml1: TXMLDocument
    Left = 632
    Top = 264
    DOMVendorDesc = 'MSXML'
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 680
    Top = 264
  end
end
